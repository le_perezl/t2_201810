package model.data_structures;

public class Nod<T> {

	private Nod<T> back;
	private Nod<T> next;
	private T actual;
	
	public Nod(T elemento) {
		actual= elemento;
		next=null;
	}
	
	public T getActual() {
		return actual;
	}
	
	public Nod<T> getNext(){
		return next;
	}
	public Nod<T> getBack(){
		return back;
	}
	
	public void switchNext(Nod<T> newN){
		next= newN;
		
	}
	public void switchBack(Nod<T> newN){
		back= newN;
		
	}
}
