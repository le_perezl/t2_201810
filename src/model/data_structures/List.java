package model.data_structures;

public class List<T extends Comparable<T>> implements LinkedList<T> {

	private Nod<T> prim;
	
	private Nod<T> ult;
	
	private int size;
	
	public List(){
		prim = null;
		ult = null;
		size = 0;
	}
	
	
	@Override
	public void add(T el) {
		// TODO Auto-generated method stub
		Nod<T> newN= new Nod<T>(el);
		if( prim==null) {
			prim= newN;
			ult= newN;
		}
		else {
			ult.switchNext(newN);
			ult.getNext().switchBack(ult);
			ult=newN;
			size++;
		}
	}

	@Override
	public void delete(T el) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T get(T el) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}
